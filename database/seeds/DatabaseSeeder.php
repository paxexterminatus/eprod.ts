<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder {
    public function run() {
        //Category
        $containers = App\Category::create([
            'over_id' => null,
            'type' => 'main',
            'name' => 'Мусорные контейнеры' ,
            'text' => 'Мусорные контейнеры...',
            'props_map' => '{}',
            'title' => 'sub seo title',
            'note' => 'sub seo note',
            'header' => 'sub seo h1',
            'slug' => 'garbage-containers',
        ]);

        $urns = App\Category::create([
            'over_id' => $containers->id,
            'type' => 'sub',
            'name' => 'Урны',
            'text' => 'Урны - это круто',
            'props_map' => '{}',
            'title' => 'seo title',
            'note' => 'seo note',
            'header' => 'seo h1',
            'slug' => 'urns',
        ]);

        $constructionWaste = App\Category::create([
            'over_id' => $containers->id,
            'type' => 'sub',
            'name' => 'Строительные отходы',
            'text' => 'Контейнеры для строительных отходов...',
            'props_map' => '{}',
            'title' => 'seo title',
            'note' => 'seo note',
            'header' => 'Контейнеры для строительных отходов',
            'slug' => 'construction-waste',
        ]);

        /////2
        $furniture = App\Category::create([
            'over_id' => null,
            'type' => 'main',
            'name' => 'Металлическая мебель',
            'text' => 'Легкая и дешевая мебель из металла',
            'props_map' => '{}',

            'title' => 'seo title',
            'note' => 'seo note',
            'header' => 'seo h1',
            'slug' => 'furniture',
        ]);

        $tables = App\Category::create([
            'over_id' => $furniture->id,
            'type' => 'sub',
            'name' => 'Столы',
            'text' => 'Столы это просто',
            'props_map' => '{}',

            'title' => 'seo title',
            'note' => 'seo note',
            'header' => 'seo h1',
            'slug' => 'tables',
        ]);
        //Items
        $urn = App\Item::create([
            'name' => 'Урна ТБО-1',
            'text' => 'Описание урны',
            'props' => '{}',
            'title' => 'Урна ТБО-1',
            'note' => 'Урна ТБО-1...',
            'header' => 'Урна ТБО-1',
            'slug' => 'Urna-TBO-1',
        ]);

        $container = App\Item::create([
            'name' => 'Контейнер для мусора 5000',
            'text' => 'Строительный контейнер для мусора 5000...',
            'props' => '{}',
            'title' => 'Строительный контейнер для мусора 5000 | Производство, покраста и доставка',
            'note' => 'Строительный контейнер для мусора 5000...',
            'header' => 'Строительный контейнер для мусора 5000',
            'slug' => 'musornyy-konteyner-5000',
        ]);
        //category and items

        DB::table('category_item')->insert([
            'category_id' => $urns->id,
            'item_id' => $urn->id
        ]);

        DB::table('category_item')->insert([
            'category_id' => $constructionWaste->id,
            'item_id' => $container->id
        ]);
    }
}
