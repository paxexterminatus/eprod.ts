<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableCreateFileBind extends Migration {
    public function up() {
        Schema::create('file_bind', function (Blueprint $table) {
            $table->integer('id',true,true);
            $table->integer('file_id',false,true);
            //relation
            $table->integer('item_id',false,true);
            $table->string('item_type',25);
            //pack
            $table->string('bundle',25);
            $table->integer('index',false,true);

            //keys
            $table->foreign('file_id')->references('id')->on('file');
        });
    }

    public function down() {}
}
