<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableCreateCategory extends Migration {
    public function up() {
        Schema::create('category', function (Blueprint $table) {
            //relations
            $table->integer('id',true,true);
            $table->integer('over_id', false, true)->nullable();
            $table->string('type'); //main, sub, pack
            //data
            $table->string('name',40);
            $table->text('text');
            $table->text('props_map')->nullable();
            //seo
            $table->string('title',80)->nullable();
            $table->string('note',200)->nullable();
            $table->string('header',40)->nullable();
            $table->string('slug',25);
            //times
            $table->timestamps();

            //keys
            $table->foreign('over_id')->references('id')->on('category');
            $table->unique('slug');
        });
    }
    public function down() {
        Schema::drop('category');
    }
}
