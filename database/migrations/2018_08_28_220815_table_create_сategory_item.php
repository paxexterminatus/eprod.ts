<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableCreateсategoryItem extends Migration {
    public function up() {
        Schema::create('category_item', function (Blueprint $table) {
            $table->integer('category_id',false,true);
            $table->integer('item_id',false,true);

            $table->primary(array('category_id', 'item_id'));
            $table->foreign('category_id')->references('id')->on('category');
            $table->foreign('item_id')->references('id')->on('item');
        });
    }
    public function down() {
        Schema::drop('category_item');
    }
}
