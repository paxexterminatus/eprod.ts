<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableCreateFile extends Migration {
    public function up() {
        Schema::create('file', function (Blueprint $table) {
            $table->integer('id',true,true);
            //seo
            $table->string('name',250);
            $table->string('alt',120);
            $table->string('title',120);

            $table->string('path',250);
            $table->string('type',250);
            $table->string('format',250);
            $table->integer('size',false,true);
            //times
            $table->timestamps();
            //keys
            $table->unique('name');
        });
    }

    public function down() {}
}
