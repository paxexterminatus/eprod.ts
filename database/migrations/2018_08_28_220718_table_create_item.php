<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableCreateItem extends Migration {
    public function up() {
        Schema::create('item', function (Blueprint $table) {
            $table->integer('id',true,true);
            //base
            $table->string('name',40);
            $table->text('text');
            $table->text('props')->nullable();
            //seo
            $table->string('title',80)->nullable();
            $table->string('note',200)->nullable();
            $table->string('header',40)->nullable();
            $table->string('slug',25);
            //times
            $table->timestamps();

            //keys
            $table->unique('slug');
        });
    }
    public function down()
    {
        Schema::drop('item');
    }
}
