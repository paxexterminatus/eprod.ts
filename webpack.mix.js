let mix = require('laravel-mix');

mix
    .sourceMaps()
    .js('resources/app/app.js', 'public/js')
    .js('resources/adm/adm.js', 'public/js')
    .sass('resources/sass/app.sass', 'public/css')
    .sass('resources/sass/adm.sass', 'public/css')
    .browserSync('localhost')
    .webpackConfig({ devtool: "source-map" })
;
