<?php

Route::get(     '/'                  ,['as'=>'index'          ,'uses'=>'AppController@index']);
Route::get(     'contacts'           ,['as'=>'contacts'       ,'uses'=>'AppController@contacts']);
Route::get(     'products/{slug?}'   ,['as'=>'products'       ,'uses'=>'AppController@products']);
Route::get(     'services'           ,['as'=>'services'       ,'uses'=>'AppController@services']);

Route::get(     'product/{slug}'     ,['as'=>'product'        ,'uses'=>'ProductController@product']);

Route::get(     'adm'                ,['as'=>'adm.index'      ,'uses'=>'Admin\AdminController@index']);
Route::get(     'adm/products'       ,['as'=>'adm.products'   ,'uses'=>'Admin\AdminController@products']);
Route::get(     'adm/categories'     ,['as'=>'adm.categories' ,'uses'=>'Admin\AdminController@categories']);
Route::get(     'adm/files'         ,['as'=>'adm.files'      ,'uses'=>'Admin\AdminController@files']);

Route::get(     'adm/category/{id}'  ,['as'=>'adm.cat'        ,'uses'=>'Admin\AdminCategoryController@get']);
Route::post(    'adm/category'       ,['as'=>'adm.cat'        ,'uses'=>'Admin\AdminCategoryController@save']);
Route::delete(  'adm/category/{id}'  ,['as'=>'adm.cat'        ,'uses'=>'Admin\AdminCategoryController@delete']);

Route::get(     'adm/product/{id}'   ,['as'=>'adm.prod'       ,'uses'=>'Admin\AdminProductController@get']);
Route::post(    'adm/product'        ,['as'=>'adm.prod'       ,'uses'=>'Admin\AdminProductController@save']);
Route::delete(  'adm/product/{id}'   ,['as'=>'adm.prod'       ,'uses'=>'Admin\AdminProductController@delete']);

//Route::get(     'adm/file/{id}'   ,['as'=>'adm.file'       ,'uses'=>'Admin\AdminProductController@get']);
Route::post(    'adm/file'   ,['as'=>'adm.file' ,'uses'=>'Admin\AdminFileController@save']);
Route::delete(  'adm/file'   ,['as'=>'adm.file' ,'uses'=>'Admin\AdminFileController@delete']);
