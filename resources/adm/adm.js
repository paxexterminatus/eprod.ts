window.Vue = require('vue');
//Pages
Vue.component('category-list', require('./categories/category-list'));

Vue.component('products-cmp',       require('./components/products-cmp'));
//Forms
Vue.component('form-cmp',           require('./forms/form-module'));
//Forms - Fields
Vue.component('field-id',           require('./forms/fields/id'));
Vue.component('field-see',          require('./forms/fields/see'));
Vue.component('field-time',         require('./forms/fields/time'));
Vue.component('field-string',       require('./forms/fields/string'));
Vue.component('field-number',       require('./forms/fields/number'));
Vue.component('field-text',         require('./forms/fields/text'));
Vue.component('field-select',       require('./forms/fields/select'));
Vue.component('field-markdown',     require('./forms/fields/markdown'));
Vue.component('field-fileBundle',   require('./forms/fields/fileBundle'));
//Data Forms
Vue.component('data-form',   require('./dataForms/data-form'));
Vue.component('string-field',   require('./dataForms/dataFields/string-field'));
Vue.component('view-field',   require('./dataForms/dataFields/view-field'));
Vue.component('url-view-field',   require('./dataForms/dataFields/url-view-field'));
//Data Forms Marks
Vue.component('mark-list',   require('./dataForms/markFields/mark-list'));
Vue.component('req-mark',   require('./dataForms/markFields/req-mark'));
Vue.component('seo-mark',   require('./dataForms/markFields/seo-mark'));
//Files
Vue.component('files-cmp', require('./files/files-cmp'));
Vue.component('files-image', require('./files/files-image'));
Vue.component('files-select-cmp', require('./files/files-select'));
//Elements - SVG
Vue.component('vue-svg', require('../vue/svg/vue-svg'));
Vue.component('svg-btn', require('../vue/svg/svg-btn'));
Vue.component('svg-btn-switch', require('../vue/svg/svg-btn-switch'));
Vue.component('ico-cross', require('../vue/svg/ico-cross'));
Vue.component('ico-database', require('../vue/svg/ico-database'));
Vue.component('ico-pencil', require('../vue/svg/ico-pencil'));
Vue.component('ico-eye', require('../vue/svg/ico-eye'));
import ButtonLook from '../vue/svg/ButtonLook';
//Make Classes
const make = {
    ButtonLook
};
Object.defineProperty(Vue.prototype, 'make', { value: make });

const adm = new Vue({
    el: '#adm'
});
