export const fieldPrototype = {
    props: {
        value: {},
        params: {type: Object},
        errors: {type: Array, default: ()=>{return[]}},
        entity: {type: String, default: null}
    },
    data(){return{
        val: this.value,
    }},
    watch: {
        value(val) {
            this.val = val;
        },
        val(val){
            this.$emit('input',val);
        },
    },
    computed: {
        errorFirst: function () {
            return !!this.errors[0] ? this.errors[0] : '';
        }
    }
};
