const string = 'string', text = 'text', select = 'select', see = 'see', markdown = 'markdown';

const hint = function ({type = null, list = []} = {type: null, list: []}) {
    return {type, list}
};

export default {
    name:    hint({type: string}),
    over_id: hint(),
    type:    hint(),
    text:    hint({type: markdown}),
    title:   hint({type: string}),
    subs:    hint(),
    note:    hint({type: text}),
    header:  hint({type: string}),
    slug:    hint({type: string})
}
