export default {
    product: {
        cover: {
            bundle: 'cover',
            file: 'image',
            type: 'item',
            count: {min: 1, max: 1},
        },
        gallery: {
            bundle: 'gallery',
            file: 'image',
            type: 'list',
            count: {min: 1, max: 6},
        },
    },
}
