export default {
    createMain () {return{
        id: null,
        over_id: null,
        type: 'main',
        name: 'New Category',
        text: null,
        props_map: null,
        subs: [],
        title: null,
        note: '',
        header: null,
        slug: null,
        created_at: null,
        updated_at: null,
    }},
    createSub ({over_id}) {return{
        id: null,
        over_id,
        type: 'sub',
        name: 'New Subcategory',
        text: null,
        props_map: null,
        title: null,
        note: '',
        header: null,
        slug: null,
        created_at: null,
        updated_at: null,
    }}
}
