const
    id = 'id',
    string = 'string',
    text = 'text',
    select = 'select',
    see = 'see',
    markdown = 'markdown',
    time = 'time',
    filesBundle = 'fileBundle';

const hint = function ({type = null, list = []} = {type: null, list: []}) {
    return {type, list}
};

export default {
    id:         hint({type: id}),
    name:       hint({type: string}),
    text:       hint({type: markdown}),
    props:      hint({type: string}),
    title:      hint({type: string}),
    note:       hint({type: text}),
    header:     hint({type: string}),
    slug:       hint({type: string}),
    created_at: hint({type: time}),
    updated_at: hint({type: time}),
    categories: hint(),
    textMarkdown: hint({type: see}),
    files: hint({type: filesBundle})
}
