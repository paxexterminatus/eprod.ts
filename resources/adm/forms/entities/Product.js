export default {
    create({category_id}) {return{
        id: null,
        name: 'New Product',
        text: 'New Product...',
        props: null,
        title: null,
        note: '',
        header: null,
        slug: 'new-product',
        created_at: null,
        updated_at: null,
        categories: {category_id, item_id: null}
    }},
}
