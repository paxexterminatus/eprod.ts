import {FormField, component, mark} from '../dataForms/FormFieldClass'

export default {
    name: new FormField({
        component: component.string, label: 'File name', marks: [mark.req, mark.seo]
    }),
    alt: new FormField({
        component: component.string, label: 'alt Attribute', marks: [mark.req, mark.seo]
    }),
    title: new FormField({
        component: component.string, label: 'title Attribute', marks: [mark.req, mark.seo]
    }),
    link: new FormField({
        component: component.urlView, label: 'URL',
    }),
    type: new FormField({
        component: component.view, label: 'MIME type',
    }),
    format: new FormField({
        component: component.view, label: 'File format',
    }),
    sizeKb: new FormField({
        component: component.view, label: 'Size',
    }),
}
