import axios from 'axios';

const def = {
    link: {
        src: null, href: null, text: 'not exist'
    }
};

class UserFile {
    constructor({data, cmp})
    {
        this.id = null;
        this.alt = '';
        this.title = '';
        this.name = data.name;
        this.size = data.size;
        this.type = '';
        this.format = '';
        this.created_at = '';
        this.updated_at = '';
        this.body = null;

        this.linkSet();

        this.$api = {
            url: '/adm/file',
        };
        this.$cmp = cmp;
    }
    linkSet({src, href = def.link.href, text = def.link.text} = def.link)
    {
        const target = href ? '_blank' : null;

        this.link = {
            target, src, href, text
        }
    }
    /*API*/
    apiSave({thenFn,catchFn}) {
        axios.post(this.$api.url, this.saveData)
            .then(resp =>
            {
                const data =  resp.data.dataset;
                this.id = data.id;
                this.alt = data.alt;
                this.title = data.title;
                this.name = data.name;
                this.size = data.size;
                this.type = data.type;
                this.format = data.format;
                this.created_at = data.created_at;
                this.updated_at = data.updated_at;

                this.linkSet({src: data.url, href: data.url, text: data.name});

                thenFn();
            });
    }
    get saveData()
    {
        return {dataset:{
                id: this.id,
                alt: this.alt,
                title: this.title,
                name: this.name,
                size: this.size,
                type: this.type,
                format: this.format,
                body: this.body,
            }}
    }
    get removeData()
    {
        return {data:{id: this.id}};
    }

    /*DISPLAY*/
    get sizeKb()
    {
        return (this.size ? this.size / 1000 : '0').toFixed(0) + ' Kb';
    }
}

export class UserFileJs extends UserFile
{
    constructor({uid, data, cmp})
    {
        super({data, cmp});
        this.uid = uid;

        const mime = data.type.split('/');
        this.type = mime[0];
        this.format = mime[1];

        const fileReader = new FileReader();
        fileReader.onload = () => {
            this.body = fileReader.result;
            this.linkSet({src: this.body});
        };
        fileReader.readAsDataURL(data);
    }

    save() {
        const thenFn = () => {
            console.log('saved');
            for (let i = 0; i < this.$cmp.filesNew.length; i++)
            {
                const file = this.$cmp.filesNew[i];
                if (file.id === this.id)
                {
                    this.$cmp.filesDb.unshift(file);
                    this.$cmp.filesNew.splice(i,1);
                }
            }
        };
        const catchFn = () => {};
        this.apiSave({thenFn, catchFn});
    }

    remove() {
        console.log('removed');
        this.$cmp.filesNew = this.$cmp.filesNew.filter(el => el.uid !== this.uid);
    }
}

export class UserFileDb extends UserFile {
    constructor({data, cmp}){
        super({data, cmp});

        this.id = data.id;
        this.alt = data.alt;
        this.title = data.title;
        this.type = data.type;
        this.format = data.format;
        this.created_at = data.created_at;
        this.updated_at = data.updated_at;

        this.linkSet({src: data.url, href: data.url, text: data.name});
    }

    remove() {
        const thenFn = () => {
            console.log('removed');
            this.$cmp.filesDb = this.$cmp.filesDb.filter(el => el.id !== this.id);
        };
        const catchFn = () => {};

        axios.delete(this.$api.url, this.removeData)
            .then(resp => {
                thenFn();
            });
    }
}

