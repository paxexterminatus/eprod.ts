export const dataField = {
    props: {
        value: {},
        field: {type: Object}
    },
    data(){return{
        valueData: this.value,
    }},
    watch: {
        valueData(newValue) {
            this.$emit('input',newValue);
        },
        value (newValue) {
            this.valueData = newValue;
        }
    },
};

export const dataView = {
    props: {
        value: {},
        field: {type: Object}
    },
};
