export class FormField {
    constructor({component, list, label, marks} = {}){
        if (component) this.component = component;
        if (list) this.list = list;
        this.label = label || component;
        if (marks) this.marks = marks;
    }
}

export const component = {
    string: 'string-field',
    view: 'view-field',
    urlView: 'url-view-field',
};

export const mark = {
    req: 'req',
    seo: 'seo',
};
