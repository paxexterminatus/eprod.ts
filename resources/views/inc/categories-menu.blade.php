@php
    $itSlug = $itCategory ? $itCategory->slug : null;
@endphp

@foreach($categories as $category)
    <a class="item {{ $category->slug == $itSlug ? 'it' : '' }}" href="{{route('products',['slug' => $category->slug])}}">{{$category->name}}</a>
    @foreach($category->subs as $sub)
        <a class="item sub {{ $sub->slug == $itSlug ? 'it' : '' }}" href="{{route('products',['slug' => $sub->slug])}}">{{$sub->name}}</a>
    @endforeach
@endforeach
