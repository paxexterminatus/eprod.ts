@extends('layouts.app')

@section('title', $product->title)
@section('description', $product->note)

@section('content')
    <div class="content-nav">
        @include('inc.categories-menu',['categories' => $categories, 'itCategory' => $product->categories[0]])
    </div>
    <article class="content-data">
        <header style="position: relative">
            <h1>{{$product->header}}</h1>
        </header>
        {{--<nav class="content-nav-box">--}}
            {{--<ul class="content-nav" style="visibility: visible">--}}
                {{--<li><a href="#">Галерея</a></li>--}}
                {{--<li><a href="#">Модификации</a></li>--}}
            {{--</ul>--}}
        {{--</nav>--}}
        <section class="art-text">
            <div class="content-info">
                <div class="content-text">
                    {!! $product->textMarkdown !!}
                </div>
            </div>
            https://codepen.io/wunnle/pen/ZLomgG
            <div class="content-gallery">
                <div class="gallery-item" style="background-image: url('/img/x1.jpg')"></div>
                <div class="gallery-item" style="background-image: url('/img/x1.jpg')"></div>
                <div class="gallery-item" style="background-image: url('/img/x1.jpg')"></div>
                <div class="gallery-item" style="background-image: url('/img/x1.jpg')"></div>
                <div class="gallery-item" style="background-image: url('/img/x1.jpg')"></div>
                <div class="gallery-item" style="background-image: url('/img/x1.jpg')"></div>
            </div>
            {{--<div class="content-gallery">--}}
                {{--<div class="gallery-item" style="background-image: url('/img/x1.jpg')"></div>--}}
                {{--<div class="gallery-item" style="background-image: url('/img/x1.jpg')"></div>--}}
                {{--<div class="gallery-item" style="background-image: url('/img/x1.jpg')"></div>--}}
                {{--<div class="gallery-item" style="background-image: url('/img/x1.jpg')"></div>--}}
                {{--<div class="gallery-item" style="background-image: url('/img/x1.jpg')"></div>--}}
                {{--<div class="gallery-item" style="background-image: url('/img/x1.jpg')"></div>--}}
            {{--</div>--}}
            {{--<div class="content-gallery">--}}
                {{--<div class="gallery-item" style="background-image: url('/img/x1.jpg')"></div>--}}
                {{--<div class="gallery-item" style="background-image: url('/img/x1.jpg')"></div>--}}
                {{--<div class="gallery-item" style="background-image: url('/img/x1.jpg')"></div>--}}
                {{--<div class="gallery-item" style="background-image: url('/img/x1.jpg')"></div>--}}
                {{--<div class="gallery-item" style="background-image: url('/img/x1.jpg')"></div>--}}
                {{--<div class="gallery-item" style="background-image: url('/img/x1.jpg')"></div>--}}
            {{--</div>--}}
            {{--<div class="content-gallery">--}}
                {{--<div class="gallery-item" style="background-image: url('/img/x1.jpg')"></div>--}}
                {{--<div class="gallery-item" style="background-image: url('/img/x1.jpg')"></div>--}}
                {{--<div class="gallery-item" style="background-image: url('/img/x1.jpg')"></div>--}}
                {{--<div class="gallery-item" style="background-image: url('/img/x1.jpg')"></div>--}}
                {{--<div class="gallery-item" style="background-image: url('/img/x1.jpg')"></div>--}}
                {{--<div class="gallery-item" style="background-image: url('/img/x1.jpg')"></div>--}}
            {{--</div>--}}
            {{--<div class="content-gallery">--}}
                {{--<div class="gallery-item" style="background-image: url('/img/x1.jpg')"></div>--}}
                {{--<div class="gallery-item" style="background-image: url('/img/x1.jpg')"></div>--}}
                {{--<div class="gallery-item" style="background-image: url('/img/x1.jpg')"></div>--}}
                {{--<div class="gallery-item" style="background-image: url('/img/x1.jpg')"></div>--}}
                {{--<div class="gallery-item" style="background-image: url('/img/x1.jpg')"></div>--}}
                {{--<div class="gallery-item" style="background-image: url('/img/x1.jpg')"></div>--}}
            {{--</div>--}}
            {{--<div class="content-gallery">--}}
                {{--<div class="gallery-item" style="background-image: url('/img/x1.jpg')"></div>--}}
                {{--<div class="gallery-item" style="background-image: url('/img/x1.jpg')"></div>--}}
                {{--<div class="gallery-item" style="background-image: url('/img/x1.jpg')"></div>--}}
                {{--<div class="gallery-item" style="background-image: url('/img/x1.jpg')"></div>--}}
                {{--<div class="gallery-item" style="background-image: url('/img/x1.jpg')"></div>--}}
                {{--<div class="gallery-item" style="background-image: url('/img/x1.jpg')"></div>--}}
            {{--</div>--}}
        </section>
        <footer class="art-footer">
            <time class="art-time" datetime="{{$product->updated_at}}">{{$product->updated_at->format('M Y')}}</time>
        </footer>
    </article>
@endsection
