@extends('layouts.adm')

@php
/**
 * @var \App\Category[] $categories
 * @var \App\Category $category
 */
@endphp
@section('content')
    <div class="content page">
        <category-list :categories="{{ $categories }}"/>
    </div>
@endsection
