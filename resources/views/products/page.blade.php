@php
/**
 * @var \App\Category[] $categories
 * @var \App\Category $category
 */
@endphp
<section>
    <h1>Продукция предприятия</h1>
    <ul>
    @foreach($categories as $category)
        <li>{{$category->header}}</li>

        @if(count($category->subs))
            @foreach($category->subs as $sub)
                <li class="sub">{{$sub->header}}</li>
            @endforeach
        @endIf

    @endforeach
    </ul>
</section>

