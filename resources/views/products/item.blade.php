@php
/**
 * @var \App\Item $item
 */
@endphp
<article class="item">
    <header>
        <a class="item-link" href="{{route('product',['slug' => $item->slug])}}">{{$item->name}}</a>
    </header>
    <p class="item-note">{{$item->note}}</p>
</article>
