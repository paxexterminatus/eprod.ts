@php
/**
 * @var \App\Category $category
 */
@endphp
<h1>{{$category->header}}</h1>

@if(count($category->items))
    @foreach($category->items as $it)
        @include('products.item',['item' => $it])
    @endforeach
@endIf

@if(count($category->subs))
    @foreach($category->subs as $sub)
        <h2>{{$sub->header}}</h2>

        @if(count($sub->items))
            @foreach($sub->items as $it)
                @include('products.item',['item' => $it])
            @endforeach
        @endIf

    @endforeach
@endIf
