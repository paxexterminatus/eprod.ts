@php
/**
 * @var \App\Category[] $categories
 * @var \App\Category $category
 */
@endphp
<div class="content-menu">
    @foreach ($categories as $category)
        <a class="menu-item first after" href="{{route('categories') .'/'. $category->slug}}">{{ $category->name }}</a>
        @foreach ($category->sub as $it)
            <a class="menu-item-2" href="/products/{{$it->slug}}">{{ $it->name }}</a>
        @endforeach
    @endforeach
</div>
