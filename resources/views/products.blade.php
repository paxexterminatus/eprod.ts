@php
/**
 * @var \App\Category[] $categories
 * @var \App\Category $itCategory
 */
@endphp
@extends('layouts.app')

@section('title', 'Продукция')

@section('content')
    <div class="content">
        <div class="content-menu">
            @include('inc.categories-menu',compact($categories, $itCategory))
        </div>
        <div class="content-data">
            @if($itCategory)
                @include('products.data',['category' => $itCategory])
            @else
                @include('products.page',compact($categories))
            @endif
        </div>
    </div>
@endsection
