<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Admin Panel</title>

    <link rel="stylesheet" type="text/css" href="{{ asset('css/adm.css') }}">
</head>
<body>
<div id="adm" class="app">
    @section('sidebar')
        <nav class="nav-top">
            <a class="nav-link" href="{{route('adm.index')}}">Main</a>
            <a class="nav-link" href="{{route('adm.categories')}}">Categories</a>
            <a class="nav-link" href="{{route('adm.products')}}">Products</a>
            <a class="nav-link" href="{{route('adm.files')}}">Files</a>
        </nav>
    @show

    @yield('content')
</div>
<script src="{{ asset('js/adm.js') }}"></script>
</body>
</html>
