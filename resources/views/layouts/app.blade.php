<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>@yield('title') | МолПроПартнер</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="@yield('description')">

    <link rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}">
</head>
<body>
<div id="app" class="app">
    <nav class="nav-top">
        <a class="nav-link" href="{{route('index')}}">Главная</a>
        <a class="nav-link" href="{{route('products')}}">Продукция</a>
    </nav>

    <div  class="content ">
        @yield('content')
    </div>

    <footer class="site-footer">
        site-footer
    </footer>
</div>
<script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
