export const dragAndDrop = {
    data(){return{
        dragBoxCss:{dragover: false},
        afterDropFun  : function (event) {},
        afterLeaveFun : function (event) {},
        afterOverFun  : function (event) {},
    }},
    methods: {
        dragOver(event) {
            event.stopPropagation();
            event.preventDefault();
            this.dragBoxCss.dragover = true;
            this.afterOverFun(event);
        },
        dragLeave(event) {
            event.stopPropagation();
            event.preventDefault();
            this.dragBoxCss.dragover = false;
            this.afterLeaveFun(event);
        },
        drop(event) {
            event.stopPropagation();
            event.preventDefault();
            this.dragBoxCss.dragover = false;
            this.afterDropFun(event);
        },
    },
};
