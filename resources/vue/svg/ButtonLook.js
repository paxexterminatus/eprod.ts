export default class ButtonLook {
    constructor({ico, text, cssClass} = {})
    {
        this.ico = ico;
        this.text = text;
        this.cssClass = cssClass;
    }
}
