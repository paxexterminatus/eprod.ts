<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
use Michelf\Markdown;
/**
 * Class Item
 * @package App
 *
 * @property integer id
 * @property string name
 * @property string text
 * @property string props
 *
 * @property string title
 * @property string note
 * @property string header
 * @property string slug
 *
 * @property \DateTime created_at
 * @property \DateTime updated_at
 *
 * @property string textMarkdown
 *
 * @property Category[] categories
 */
class Item extends Model {
    protected $table = 'item';
    protected $guarded = ['id','created_at','updated_at'];
    protected $appends = ['textMarkdown'];
    protected $dates = ['created_at','updated_at'];

    public function categories(){
        return $this->belongsToMany(Category::class)
            ->withPivot('category_id','item_id');
    }
    public function files(){
        return $this->morphToMany(File::class,'item','file_bind')->withPivot('bundle', 'index');
    }
    public function getTextMarkdownAttribute(){
        return Markdown::defaultTransform($this->text);
    }

}
