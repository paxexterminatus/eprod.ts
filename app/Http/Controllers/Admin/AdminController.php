<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Http\Controllers\Controller;
use App\File;

class AdminController extends Controller {

    public function index(){
        return view('adm.index');
    }

    public function categories(){
        $categories = Category::where('type','main')->with('subs')->get();
        return view('adm.categories', ['categories' => $categories]);
    }

    public function products(){
        $categories = Category::where('type','main')
            ->with([
                'subs',
                'items',
                'subs.items',
                'subs.items.files',
            ])->get();

        return view('adm.products', ['categories' => $categories]);
    }

    public function files(){
        $files = File::orderBy('id', 'desc')->get();
        return view('adm.files',['files' => $files]);
    }
}
