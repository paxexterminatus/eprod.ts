<?php

namespace App\Http\Controllers\Admin;

use App\File;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Classes\UserFile;
use Illuminate\Support\Facades\Storage;

class AdminFileController extends Controller {

    public function save(Request $request)
    {
        $userFile = UserFile::make()->fillApiDataset($request->input('dataset'));
        $userFile->save();
        return $userFile->response();
    }
    public function get(Request $request)
    {
        dd('file get');
    }
    public function delete(Request $request)
    {
        $fileId = $request->input('id');
        $file = File::find($fileId);
        $deleted = $file->delete();

        if ($deleted)
        {
            Storage::delete($file->path);
        }
    }
}
