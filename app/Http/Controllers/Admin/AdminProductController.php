<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ProductSaveRequest;
use App\Item;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class AdminProductController extends Controller {

    public function get(Request $request) {

    }

    public function save(ProductSaveRequest $request) {
        /**
         * @var Item $product
         */
        $dataApi = $request->validated()['item'];
        $id = $dataApi['id'] ?? null;

        if ($id >= 1) {
            Item::where('id', $id)
                ->update([
                    'name'      => $dataApi['name'],
                    'text'      => $dataApi['text'],
                    'props'     => $dataApi['props'],
                    'title'     => $dataApi['title'],
                    'note'      => $dataApi['note'],
                    'header'    => $dataApi['header'],
                    'slug'      => $dataApi['slug'],
                ]);

            $product = Item::find($id);
        } else {
            $product = Item::create([
                'name'      => $dataApi['name'],
                'text'      => $dataApi['text'],
                'props'     => $dataApi['props'],
                'title'     => $dataApi['title'],
                'note'      => $dataApi['note'],
                'header'    => $dataApi['header'],
                'slug'      => $dataApi['slug'],
            ]);
            DB::table('category_item')->insert([
                'category_id' => $dataApi['categories']['category_id'],
                'item_id' => $product->id
            ]);
        }

        return response()->json([
            'message' => 'Product saved',
            'dataset' => $product
        ]);
    }

    public function delete(Request $request, $id) {

    }
}
