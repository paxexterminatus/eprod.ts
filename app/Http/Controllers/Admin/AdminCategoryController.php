<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Http\Requests\CategorySaveRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminCategoryController extends Controller {

    public function get(Request $request, $id) {
        dd($id);
    }

    public function save(CategorySaveRequest $request) {
        $dataApi = $request->validated()['category'];
        
        $id = $dataApi['id'] ?? null;

        if ($id >= 1) {
            Category::where('id', $id)
                ->update([
                    'name'      => $dataApi['name'],
                    'text'      => $dataApi['text'],
                    'props_map' => $dataApi['props_map'],
                    'title'     => $dataApi['title'],
                    'note'      => $dataApi['note'],
                    'header'    => $dataApi['header'],
                    'slug'      => $dataApi['slug'],
                ]);

            $dataDB = Category::find($id);
        } else {
            $dataDB = Category::create([
                'type'      => $dataApi['type'],
                'over_id'   => $dataApi['over_id'],
                'name'      => $dataApi['name'],
                'text'      => $dataApi['text'],
                'props_map' => $dataApi['props_map'],
                'title'     => $dataApi['title'],
                'note'      => $dataApi['note'],
                'header'    => $dataApi['header'],
                'slug'      => $dataApi['slug'],
            ]);
        }

        return response()->json([
            'message' => 'Category saved',
            'dataset' => $dataDB
        ]);
    }

    public function delete(Request $request, $id) {

    }
}
