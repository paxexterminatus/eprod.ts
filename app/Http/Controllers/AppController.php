<?php

namespace App\Http\Controllers;

use App\Category;

class AppController extends Controller {

    public function index() {
        return view('index');
    }

    public function contacts() {
        return view('contacts');
    }

    public function products($slug = null) {
        $categories = Category::where('type','main')->with(['subs'])->get();
        $itCategory = Category::where('slug', $slug)->first();

        return view('products', [
            'categories' => $categories,
            'itCategory' => $itCategory
        ]);
    }

    public function services() {
        return view('services');
    }
}
