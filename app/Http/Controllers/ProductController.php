<?php

namespace App\Http\Controllers;

use App\Category;
use App\Item;

class ProductController extends Controller {

    public function product($slug) {
        /**
         * @var Category[] $categories
         * @var Item $product
         */
        $product = Item::where('slug',$slug)->with(['categories'])->first();
        $categories = Category::where('type','main')->with(['subs','items','subs.items'])->get();

        return view('product', [
            'categories' => $categories,
            'product' => $product
        ]);
    }
}
