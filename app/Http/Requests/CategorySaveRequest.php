<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CategorySaveRequest extends FormRequest {
    public function authorize(){
        return true; //todo set ??? after add auth system
    }

    public function rules(){
        $entity = 'category';
        return [
            "$entity.name"      => 'required|max:40',
            "$entity.title"     => 'max:80',
            "$entity.note"      => 'max:200',
            "$entity.header"    => 'max:40',
            "$entity.slug"      => 'required|max:25',
        ];
    }
}
