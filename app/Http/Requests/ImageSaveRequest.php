<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ImageSaveRequest extends FormRequest {
    public function authorize() {
        return true; //todo set ??? after add auth system
    }

    public function rules() {
        $entity = 'file';
        return [
            "$entity.name"      => 'required|max:40',
            "$entity.title"     => 'required|max:80',
            "$entity.alt"       => 'required|max:40',
        ];
    }
}
