<?php
namespace App\Classes;

use App\File;
use Illuminate\Support\Facades\Storage;

class UserFile {
    var $id;
    var $name;
    var $alt;
    var $title;
    var $type;
    var $format;
    var $size;
    var $body;

    var $storageDir;

    var $responseMessage;
    var $responseData;

    static function make()
    {
     return new self();
    }

    function nameCast($v)
    {
        return strtolower($v);
    }

    function fillApiDataset ($dataset)
    {
        $this->id = $dataset['id'];
        $this->name = $this->nameCast($dataset['name']);
        $this->alt = $dataset['alt'];
        $this->title = $dataset['title'];
        $this->type = $dataset['type'];
        $this->format = $dataset['format'];
        $this->size = $dataset['size'];
        $this->body = $dataset['body'];

        if ($this->type === 'image')
        {
            $this->storageDir = 'public/i/';
        }
        else
        {
            $this->storageDir = 'public/f/';
        }

        return $this;
    }

    public function save()
    {
        if ($this->id) $this->update(); else $this->store();
        return $this;
    }

    private function store()
    {
        $fileBase64 = explode(',',$this->body)[1];

        $storagePath = $this->storageDir.$this->name;
        $storageResult = Storage::put($storagePath,base64_decode($fileBase64),'public');

        if($storageResult)
        {
            $file = File::create([
                'name' => $this->name,
                'alt' => $this->alt,
                'title' => $this->title,
                'path' => $storagePath,
                'type' => $this->type,
                'format' => $this->format,
                'size' => $this->size
            ]);
            if ($file)
            {
                $this->responseMessage = 'File created';
                $this->responseData = $file;
            }
        }
    }

    private function update()
    {
        $file = File::find($this->id);
        $fileHasChanged = false;
        if ($this->name !== $file->name)
        {
            $pathNew = $this->storageDir.$this->name;
            $pathCurrent = $file->path;

            $storageResult = Storage::move($pathCurrent, $pathNew);
            if ($storageResult)
            {
                $file->name = $this->name;
                $file->path = $pathNew;
                $fileHasChanged = true;
            }
        }
        if ($this->alt !== $file->alt)
        {
            $file->alt = $this->alt;
            $fileHasChanged = true;
        }
        if ($this->title !== $file->title)
        {
            $file->title = $this->title;
            $fileHasChanged = true;
        }

        if ($fileHasChanged)
        {
            $file->save();

            $this->responseMessage = 'File updated';
            $this->responseData = $file;
        }
    }

    function response()
    {
        return response()->json([
            'message' => $this->responseMessage,
            'dataset' => $this->responseData
        ]);
    }
}


