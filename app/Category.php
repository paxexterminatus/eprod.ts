<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
/**
 * Class Category
 * @package App
 * @property integer id
 * @property integer over_id
 * @property string type
 * @property string name
 * @property string text
 * @property string props_map
 * @property string title
 * @property string note
 * @property string header
 * @property string slug
 *
 * @property Item[] $items
 * @property Category[] $subs
 */
class Category extends Model {
    protected $table = 'category';
    protected $guarded = ['id','created_at','updated_at'];
    public $timestamps = true;

    public function subs() {
        return $this->hasMany(Category::class, 'over_id');
    }
    public function items() {
        return $this->belongsToMany(Item::class)
            ->as('categories')
            ->withPivot('category_id','item_id')
            ;
    }
}
