<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

/**
 * Class Item
 * @package App
 *
 * @property integer id
 *
 * @property string name
 * @property string alt
 * @property string title
 *
 * @property string path
 * @property string type
 * @property string format
 * @property int size
 *
 * @property \DateTime created_at
 * @property \DateTime updated_at
 */
class File extends Model {
    protected $table = 'file';
    protected $guarded = ['id','created_at','updated_at'];
    protected $dates = ['created_at','updated_at'];

    protected $appends = ['url'];

    public function getUrlAttribute(){
        return Storage::url($this->path);
    }
}
