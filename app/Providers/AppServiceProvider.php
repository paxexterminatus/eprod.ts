<?php

namespace App\Providers;

use App\Item;
use App\Category;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider {
    public function boot() {
        Relation::morphMap([
            'category' => Category::class,
            'item' => Item::class,
        ]);
    }

    public function register() {

    }
}
